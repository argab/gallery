module.exports = () => {
    $.gulp.task('bsync', function() {
        $.bsync.init({
            server: {
                baseDir: 'app'
            },
            notify: false
        });
    });
};