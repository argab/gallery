module.exports = () => {
    $.gulp.task('watch', () => {
        $.gulp.watch('src/pug/pages/**/*.pug', $.gulp.series('pug'));
        $.gulp.watch('src/stylus/**/*.styl', $.gulp.series('css'));
        $.gulp.watch('src/ts/**/*.ts', $.gulp.series('ts'));
    })
};