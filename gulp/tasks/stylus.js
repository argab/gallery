module.exports = () => {
    $.gulp.task('css', () => {
        return $.gulp.src('src/stylus/**/*.styl')
            .pipe($.gp.stylus({
                'include css': true
            }))
            .pipe($.gp.autoprefixer({
                browsers: ['last 10 versions'],
                cascade: false
            }))
            .pipe($.gp.csso())
            .pipe($.gulp.dest('app/css'))
            .pipe($.bsync.reload({stream: true}))
    });
};
