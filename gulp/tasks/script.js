
module.exports = () => {
$.gulp.task('js', function () {
    return $.browserify(['src/js/app.js', 'src/class/ImageLib.js'])
        .transform($.babelify)
        .bundle()
        .pipe($.source('app.js'))
        .pipe($.buffer())
        .pipe($.gp.jshint())
        .pipe($.gp.jshint.reporter('default'))
        .pipe($.gp.concat('app.min.js'))
        .pipe($.gp.uglify())
        .pipe($.gulp.dest('app/js/'))
        .pipe($.buffer())
        .pipe($.bsync.reload({
            stream:true
        }))
});
};
