
module.exports = () => {
$.gulp.task('ts', function () {
    return $.browserify({
        basedir: '.',
        debug: true,
        entries: ['src/js/app.js'],
        cache: {},
        packageCache: {}
    })
        .plugin($.tsify)
        .bundle()
        // .transform('babelify', {
        //     presets: ['es2015'],
        //     extensions: ['.ts', '.js']
        // })
        .pipe($.source('app.js'))
        .pipe($.buffer())
        .pipe($.gp.jshint())
        .pipe($.gp.jshint.reporter('default'))
        .pipe($.gp.concat('app.min.js'))
        .pipe($.gp.uglify())
        .pipe($.gulp.dest('app/js/'))
        .pipe($.buffer())
        .pipe($.buffer())
        .pipe($.bsync.reload({
            stream:true
        }))
});
};
