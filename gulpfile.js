'use strict';

global.$ = {
    gulp: require('gulp'),
    gp: require('gulp-load-plugins')(),
    bsync: require('browser-sync').create(),
    jshint: require('jshint'),
    babel: require('gulp-babel'),
    browserify: require('browserify'),
    babelify: require('babelify'),
    source: require('vinyl-source-stream'),
    buffer: require('vinyl-buffer'),
    ts: require("gulp-typescript"),
    tsify: require("tsify"),

    path: {
        tasks: [
            './gulp/tasks/pug',
            './gulp/tasks/serve',
            './gulp/tasks/stylus',
            './gulp/tasks/watch',
            // './gulp/tasks/script',
            './gulp/tasks/ts',
        ]
    }
};

$.tsProject = $.ts.createProject("tsconfig.json");

$.path.tasks.forEach(function (taskPath) {
    require(taskPath)()
});

$.gulp.task('css', $.gulp.series(
    $.gulp.parallel('pug', 'css'),
    $.gulp.parallel('watch', 'bsync')
));

$.gulp.task('def', $.gulp.series(
    $.gulp.parallel('pug', 'css', 'ts'),
    $.gulp.parallel('watch', 'bsync')
));
