import { GalleryLoader, gallery } from "./../ts/GalleryLoader";

window.addEventListener('load', function ()
{
    let loader = new GalleryLoader();

    let render = (item) => {
        return '<li>'
            + '<img src="'
            + item.thumbnailUrl + '" alt="'
            + item.title + '" title="'
            + item.title + '" />'
            + '</li>';
    };

    gallery.setParams({
        page: 275,
        pageSize: loader.pageSize * 2,
        endpoint: 'https://jsonplaceholder.typicode.com/photos',
    });

    loader.container = document.getElementById('gal');

    loader.btn = document.getElementById('btn-load-gal');

    loader.render(render);

    loader.btn.addEventListener('click', () => {loader.render(render)});
});
