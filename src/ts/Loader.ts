import { injectable } from "inversify";
import "reflect-metadata";
import { ILoader } from "./interfaces";
import axios from "axios";

@injectable()

class Loader implements ILoader
{
    protected endpoint: string;

    setEndpoint(endpoint: string): void
    {
        this.endpoint = endpoint;
    }

    async requestGet(params: object = {}): Promise<any>
    {
        if (this.endpoint === "")

            throw new Error('The `endpoint` property is not set.');

        return await axios.get(this.endpoint, {params: params});
    }
}

export { Loader };