
import { gallery } from "./app";

const GalleryLoader = function () {

    this.pageSize = 9;

    this.items = [];

    this.container = null;

    this.btn = null;

    this.render = (renderItem: (item: any) => any) => {

        this.toggleBtn(true);

        if (this.items.length === 0)
        {
            gallery.fetchLibrary((response: any) => {

                this.items = response.data;

                console.log({itemsCount: this.items.length, items: this.items});

                this.loopItems(renderItem)
            });
        }
        else
        {
            this.loopItems(renderItem)
        }

        this.toggleBtn();

        return true;
    };

    this.loopItems = (renderItem: (item: any) => any) => {

        let ln = this.items.length;

        if (ln === 0)
        {
            this.toggleBtn(false, true);

            return false;
        }

        for (let i = 0; i < ln; ++i)
        {
            if (i === this.pageSize)

                break;

            this.container.insertAdjacentHTML('beforeend', renderItem(this.items[i]));
        }

        this.items.splice(0, this.pageSize);

        if (ln < this.pageSize)
        {
            this.toggleBtn(false, true);

            return false;
        }

        return true;
    };

    this.toggleBtn = (loading: boolean = false, rm: boolean = false) => {

        if (rm)
        {
            this.btn.parentNode.removeChild(this.btn);

            return false;
        }

        if (loading)
        {
            this.btn.innerHTML = 'Загрузка...';

            this.btn.disabled = true;

            return false;
        }

        this.btn.innerHTML = 'Загрузить еще';

        this.btn.disabled = false;
    }
};

export { gallery, GalleryLoader };