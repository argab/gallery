import { ImageLib } from "./ImageLib";
import {IGallery, ILoader, IPaginator} from "./interfaces";
import {inject, injectable} from "inversify";
import "reflect-metadata";
import {TYPES} from "./types";

@injectable()

class Gallery extends ImageLib implements IGallery
{
    private _loader: ILoader;
    private _paginator: IPaginator;

    protected param = {
        page: 0,
        pageSize: 0,
        endpoint: ""
    };

    constructor(@inject(TYPES.Loader) loader: ILoader, @inject(TYPES.Paginator) paginator: IPaginator)
    {
        super();

        this._loader = loader;

        this._paginator = paginator;
    }

    setParams(params: object = {})
    {
        this.param = (<any>Object).assign(this.param, params);

        this.getLoader().setEndpoint(this.param.endpoint);

        this.getPaginator().setPage(this.param.page);

        this.getPaginator().setPageSize(this.param.pageSize);

        return this;
    }

    getLoader(): ILoader
    {
        return this._loader;
    }

    getPaginator(): IPaginator
    {
        return this._paginator;
    }

    fetchOffset(): number
    {
        this.getPaginator().setPage(this.param.page += 1);

        return this.getPaginator().fetchOffset();
    }

    getOffset(): number
    {
        return this.getPaginator().getOffset();
    }

    getLimit(): number
    {
        return this.param.pageSize;
    }
}

export { Gallery };