import { injectable } from "inversify";
import "reflect-metadata";
import { IPaginator } from "./interfaces";

@injectable()

class Paginator implements IPaginator
{
    protected page: number = 0;

    protected pageSize: number;

    protected offset: number = 0;

    setPage(page: number): void
    {
        this.page = page;
    }

    setPageSize(size: number): void
    {
        this.pageSize = size;
    }

    fetchOffset(): number
    {
        return (this.offset = this.page === 1 ? 0 : this.page * this.pageSize - this.pageSize);
    }

    getOffset(): number
    {
        return this.offset;
    }
}

export {Paginator};