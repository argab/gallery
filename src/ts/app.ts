import { container } from "./inversify.config";
import { TYPES } from "./types";
import { IGallery } from "./interfaces";

const gallery = container.get<IGallery>(TYPES.Gallery);

export { gallery };