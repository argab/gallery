import { Container } from "inversify";
import { TYPES } from "./types";
import {IGallery, ILoader, IPaginator} from "./interfaces";
import { Gallery } from "./Gallery";
import {Loader} from "./Loader";
import {Paginator} from "./Paginator";

const container = new Container();

container.bind<IGallery>(TYPES.Gallery).to(Gallery);
container.bind<ILoader>(TYPES.Loader).to(Loader);
container.bind<IPaginator>(TYPES.Paginator).to(Paginator);

export { container };