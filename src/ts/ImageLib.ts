import { ILoader, IPaginator } from "./interfaces";
import {injectable} from "inversify";
import "reflect-metadata";

@injectable()

abstract class ImageLib
{
    abstract getLoader(): ILoader;

    abstract fetchOffset(): number;

    abstract getOffset(): number;

    abstract getLimit(): number;

    fetchLibrary(callback: (response: any) => any)
    {
        this.getLoader().requestGet({_start: this.fetchOffset(),_limit: this.getLimit()})

            .then((response: any) => {callback(response)})

            .catch((error: any) => {console.log(error)});
    }
}

export { ImageLib };