const TYPES = {
    Gallery: Symbol.for("IGallery"),
    Loader: Symbol.for("ILoader"),
    Paginator: Symbol.for("IPaginator")
};

export { TYPES };