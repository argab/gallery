export interface ILoader
{
    setEndpoint(endpoint: string): void

    requestGet(params: object): any
}

export interface IPaginator
{
    setPage(page: number): void

    setPageSize(size: number): void

    fetchOffset(): number

    getOffset(): number
}

export interface IGallery
{
    setParams(params: object): IGallery;

    fetchLibrary(callback: (any: any) => any): any
}
